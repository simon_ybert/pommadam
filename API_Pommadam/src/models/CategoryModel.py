from marshmallow import fields, Schema
from . import db


class CategoryModel(db.Model):
    """
    Category Model
    """

    # table name
    __tablename__ = 'category'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    vehicles = db.relationship("VehicleModel", backref="vehicle", lazy='dynamic')

    # class constructor
    def __init__(self, data):
        """
        Class constructor
        """
        self.name = data.get('name')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_category():
        return CategoryModel.query.all()

    @staticmethod
    def get_category_by_name(name):
        return CategoryModel.query.filter_by(name=name).first()

    @staticmethod
    def get_one_category(id):
        return CategoryModel.query.get(id)

    def __repr(self):
        return '<id {}>'.format(self.id)


class CategorySchema(Schema):
    """
    User Schema
    """
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)