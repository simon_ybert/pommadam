from flask import request, json, Response, Blueprint, g
from ..models.VehicleModel import VehicleModel, VehicleSchema
from ..models.UserModel import UserModel
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)

vehicle_api = Blueprint('vehicle', __name__)
vehicle_schema = VehicleSchema()


@vehicle_api.route('/', methods=['POST'])
@jwt_required
def create():
    """
    Create User Function
    """
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error":"Unauthorised access"}, 401)

    req_data = request.get_json()
    data, error = vehicle_schema.load(req_data)

    if error:
        return custom_response(error, 400)

    vehicle_in_db = VehicleModel.get_vehicle_by_registration(data.get('registration'))
    if vehicle_in_db:
        message = {'error': 'Vehicle already exist.'}
        return custom_response(message, 400)

    vehicle = VehicleModel(data)
    vehicle.save()

    return custom_response({'success': 'Vehicle created : {}'.format(vehicle.name)}, 200)


@vehicle_api.route('/', methods=['GET'])
@jwt_required
def get_all():
    vehicles = VehicleModel.get_all_vehicle()
    ser_vehicle = vehicle_schema.dump(vehicles, many=True).data
    return custom_response(ser_vehicle, 200)

@vehicle_api.route('/<int:vehicule_id>',methods=['GET'])
@jwt_required
def get_a_vehicle(vehicule_id):
    vehicle = VehicleModel.get_one_vehicle(vehicule_id)
    if not vehicle:
        return custom_response({'error': 'vehicle not found'}, 404)
    ser_vehicle = vehicle_schema.dump(vehicle).data
    return custom_response(ser_vehicle, 200)

@vehicle_api.route('/<int:vehicle_id>', methods=['PUT'])
@jwt_required
def update(vehicle_id):
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error":"Unauthorised access"}, 401)
    req_data = request.get_json()
    data, error = vehicle_schema.load(req_data, partial=True)
    if error:
        return custom_response(error, 400)
    vehicle = VehicleModel.get_one_vehicle(vehicle_id)
    vehicle.update(data)
    ser_category = vehicle_schema.dump(vehicle).data
    return custom_response(ser_category, 200)


@vehicle_api.route('/<int:vehicle_id>', methods=['DELETE'])
@jwt_required
def delete(vehicle_id):
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error": "Unauthorised access"}, 401)
    vehicle = VehicleModel.get_one_vehicle(vehicle_id)
    if not vehicle:
        return custom_response({'error': 'vehicle not found'}, 404)
    vehicle.delete()
    return custom_response({'message': 'deleted'}, 204)


def custom_response(res, status_code):
    """
    Custom Response Function
    """
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code
    )
