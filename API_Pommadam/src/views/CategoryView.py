from flask import request, json, Response, Blueprint, g
from ..models.CategoryModel import CategoryModel, CategorySchema
from ..models.UserModel import UserModel
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)

category_api = Blueprint('category', __name__)
category_schema = CategorySchema()


@category_api.route('/', methods=['POST'])
@jwt_required
def create():
    """
    Create User Function
    """
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error":"Unauthorised access"}, 401)

    req_data = request.get_json()
    data, error = category_schema.load(req_data)

    if error:
        return custom_response(error, 400)

    # check if user already exist in the db
    category_in_db = CategoryModel.get_category_by_name(data.get('name'))
    if category_in_db:
        message = {'error': 'Category already exist.'}
        return custom_response(message, 400)

    category = CategoryModel(data)
    category.save()

    return custom_response({'success': 'Category created : {}'.format(category.name)}, 200)


@category_api.route('/', methods=['GET'])
@jwt_required
def get_all():
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error":"Unauthorised access"}, 401)

    categories = CategoryModel.get_all_category()
    ser_category = category_schema.dump(categories, many=True).data
    return custom_response(ser_category, 200)


@category_api.route('/<int:cat_id>', methods=['GET'])
@jwt_required
def get_a_category(cat_id):
    category = CategoryModel.get_one_category(cat_id)
    if not category:
        return custom_response({'error': 'category not found'}, 404)

    ser_category = category_schema.dump(category).data
    return custom_response(ser_category, 200)


@category_api.route('/<int:cat_id>', methods=['PUT'])
@jwt_required
def update(cat_id):
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error":"Unauthorised access"}, 401)
    req_data = request.get_json()
    data, error = category_schema.load(req_data, partial=True)
    if error:
        return custom_response(error, 400)
    category = CategoryModel.get_one_category(cat_id)
    category.update(data)
    ser_category = category_schema.dump(category).data
    return custom_response(ser_category, 200)


@category_api.route('/<int:cat_id>', methods=['DELETE'])
@jwt_required
def delete(cat_id):
    if UserModel.get_user_by_name(get_jwt_identity()).roles[0].name != "Admin":
        return custom_response({"error": "Unauthorised access"}, 401)
    category = CategoryModel.get_one_category(cat_id)
    category.delete()
    return custom_response({'message': 'deleted'}, 204)


def custom_response(res, status_code):
    """
    Custom Response Function
    """
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code
    )
