from marshmallow import fields, Schema
from . import db
from .VehicleModel import VehicleModel, VehicleSchema
from .UserModel import UserModel, UserSchema


class UsageModel(db.Model):
    """
    Category Model
    """

    # table name
    __tablename__ = 'usage'

    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.DateTime, nullable=False)
    end = db.Column(db.DateTime, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    description = db.Column(db.String(128), nullable=True)
    purpose = db.Column(db.String(128), nullable=False)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id', use_alter=True, name='fk_usage_vehicle_id'))
    vehicle = db.relationship('VehicleModel', foreign_keys=vehicle_id, post_update=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', use_alter=True, name='fk_usage_user_id'))
    user = db.relationship('UserModel', foreign_keys=user_id, post_update=True)

    # class constructor
    def __init__(self, data):
        """
        Class constructor
        """
        self.start = data.get('name')
        self.end = data.get('end')
        self.active = data.get('active')
        self.description = data.get('description')
        self.purpose = data.get('purpose')
        self.vehicle_id = data.get('vehicle_id')
        self.user_id = data.get('user_id')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_usage():
        return UsageModel.query.all()

    @staticmethod
    def get_usage_by_name(name):
        return UsageModel.query.filter_by(name=name).first()

    @staticmethod
    def get_one_category(id):
        return UsageModel.query.get(id)

    def __repr(self):
        return '<id {}>'.format(self.id)


class UsageSchema(Schema):

    id = fields.Int(dump_only=True)
    start = fields.DateTime(required=True)
    end = fields.DateTime(required=True)
    active = fields.Boolean(required=True)
    description = fields.Str(required=True)
    purpose = fields.Str(required=True)
    vehicle_id = fields.Int(required=True)
    vehicle = fields.Nested(VehicleSchema, dump_only=True)
    user_id = fields.Int(required=True)
    user = fields.Nested(UserSchema, dump_only=True)
