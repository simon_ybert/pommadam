from marshmallow import fields, Schema
from . import db
from .CategoryModel import CategoryModel, CategorySchema



class VehicleModel(db.Model):
    """
    Vehicle Model
    """

    # table name
    __tablename__ = 'vehicle'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    registration = db.Column(db.String(128), nullable=False, unique=True)
    description = db.Column(db.String(128), nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)
    energy = db.Column(db.String(128), nullable=False)
    range = db.Column(db.Integer, nullable=False)
    place = db.Column(db.Integer, nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id', use_alter=True, name='fk_vehicle_category_id'))
    category = db.relationship('CategoryModel', foreign_keys=category_id, post_update=True)
    # usages = db.relationship("UsageModel", backref="usage", lazy='dynamic')



    # class constructor
    def __init__(self, data):
        """
        Class constructor
        """
        self.name = data.get('name')
        self.registration = data.get('registration')
        self.description = data.get('description')
        self.active = data.get('disable')
        self.energy = data.get('energy')
        self.range = data.get('range')
        self.place = data.get('place')
        self.category_id = data.get('category_id')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_vehicle_by_registration(registration):
        return VehicleModel.query.filter_by(registration=registration).first()

    @staticmethod
    def get_all_vehicle():
        return VehicleModel.query.all()

    @staticmethod
    def get_one_vehicle(id):
        return VehicleModel.query.get(id)

    def __repr(self):
        return '<id {}>'.format(self.id)


class VehicleSchema(Schema):
    """
    User Schema
    """
    id = fields.Int(dump_only=True)
    name = fields.Str(required=True)
    registration = fields.Str(required=True)
    description = fields.Str(required=True)
    active = fields.Boolean(required=True)
    energy = fields.Str(required=True)
    range = fields.Int(required=True)
    place = fields.Int(required=True)
    category_id = fields.Int(required=True)
    category = fields.Nested(CategorySchema, dump_only=True)
