from flask import Flask, session, g
from flask_user import login_required, SQLAlchemyAdapter, UserManager, UserMixin
from flask_jwt_extended import (
        JWTManager, jwt_required, create_access_token,
        get_jwt_identity
)


from .config import app_config
from .models import db, bcrypt
from .models.UserModel import UserModel
from .views.UserView import user_api as user_blueprint
from .views.CategoryView import category_api as category_blueprint
from .views.VehicleView import vehicle_api as vehicle_blueprint
from .views.UsageView import usage_api as usage_blueprint
import os


def create_app(env_name):
    """
    Create app
    """

    app = Flask(__name__)

    app.config.from_object(app_config[env_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'hhgaghhgsdhdhdd'
    app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET_KEY')
    bcrypt.init_app(app)
    db.init_app(app)

    app.register_blueprint(user_blueprint, url_prefix='/api/v1/users')
    app.register_blueprint(category_blueprint, url_prefix='/api/v1/category')
    app.register_blueprint(vehicle_blueprint, url_prefix='/api/v1/vehicle')
    app.register_blueprint(usage_blueprint, url_prefix='/api/v1/usage')

    db_adapter = SQLAlchemyAdapter(db, UserModel)
    user_manager = UserManager(db_adapter, app)
    jwt = JWTManager(app)


    @app.route('/', methods=['GET'])
    def index():
        """
        example endpoint
        """
        return 'Congratulations! Your first endpoint is workin'

    return app
