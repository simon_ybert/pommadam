﻿using System;

/// <summary>
/// description des objet reçu ou nvenyé à l'API.
/// </summary>
namespace apiModels
{
    [Serializable]
    public class LoginData
    {
        public string access_token;
        public Self self;
    }

    [Serializable]
    public class Self
    {
        public int id;
        public string email;
        public string name;
        public Role[] roles;
        
    }

    [Serializable]
    public class Role
    {
        public int id;
        public string name;
    }


    [Serializable]
    public class User
    {
        public int id;
        public string password;
        public string email;
        public string name;
        public Role[] roles;
    }

    [Serializable]
    public class Vehicle
    {
        public int id;
        public string name;
        public string registration;
        public string description;
        public Boolean active;
        public string energy;
        public int range;
        public int place;
        public int category_id;
        public Category category;
    }

    [Serializable]
    public class Category
    {
        public int id;
        public string name;
    }

    [Serializable]
    public class Usage
    {
        public int id;
        public String start;
        public String end;
        public User user;
        public Vehicle vehicle;
        public String purpose;
        public String description;
    }

}