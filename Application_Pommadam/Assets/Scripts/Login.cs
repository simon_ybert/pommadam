﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Proyecto26;
using apiModels;

public class Login : MonoBehaviour
{
    public InputField emailField;
    public InputField passwordField;
    // Start is called before the first frame update
    private RequestHelper currentRequest;
    void Start()
    {
        PlayerPrefs.SetString("baseUrl", "http://127.0.0.1:5000/api/v1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void LoginUser()
    {
        var mail = emailField.text.ToString();
        var pwd = passwordField.text.ToString();
        GetToken(mail, pwd);
    }

    public void GetToken(string email , string password)
    {
        currentRequest = new RequestHelper
        {
            Uri = PlayerPrefs.GetString("baseUrl") + "/users/login" ,
            Body = new User
            {
                email = email,
                password = password
            }
        };
        RestClient.Post<LoginData>(currentRequest)
        .Then(res  => LoginSuccess(res.access_token))
        .Catch(err => FailedLogin(err.ToString()));
    }

    private void LoginSuccess(string token)
    {
        PlayerPrefs.SetString("token", token);
        Debug.Log(token);
    }

    private void FailedLogin(string err)
    {
        Debug.Log(err);
    }
}